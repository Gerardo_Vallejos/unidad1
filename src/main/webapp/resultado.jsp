<%-- 
    Document   : resultado
    Created on : 27 abr. 2021, 20:36:34
    Author     : gerva
--%>

<%@page import="cl.model.Interes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Interes cal = (Interes) request.getAttribute("Interes");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="styles/style1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div>
            <h1>Usted ganará: </h1>
            <p>
            $ <%= cal.getTotal()%>
            </p>
        </div>
    </body>
</html>

