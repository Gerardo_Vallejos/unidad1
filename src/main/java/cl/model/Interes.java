/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

/**
 *
 * @author gerva
 */
public class Interes {

    private int numero1;
    private double numero2;
    private int numero3;
    private double total;
    
    /**
     * @return the numero1
     */
    public int getNumero1() {
        return numero1;
    }

    /**
     * @param numero1 the numero1 to set
     */
    public void setNumero1(int numero1) {
        this.numero1 = numero1;
    }

    /**
     * @return the numero2
     */
    public double getNumero2() {
        return numero2;
    }

    /**
     * @param numero2 the numero2 to set
     */
    public void setNumero2(double numero2) {
        this.numero2 = numero2;
    }

    /**
     * @return the numero3
     */
    public int getNumero3() {
        return numero3;
    }

    /**
     * @param numero3 the numero3 to set
     */
    public void setNumero3(int numero3) {
        this.numero3 = numero3;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }
    
     public void total() {

        this.total = this.getNumero1() * (this.getNumero2()/100) * this.getNumero3();
    }
    
}
